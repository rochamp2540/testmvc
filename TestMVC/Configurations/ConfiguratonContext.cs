﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMVC.Models;

namespace TestMVC.Configurations
{
    public class ConfiguratonContext : IConfiguratonContext
    {
        private readonly IConfiguration config;
        public ConfiguratonContext(IConfiguration config)
        {
            this.config = config;

        }
        public int GetCachingDays()
        {
            var demo1 = Convert.ToInt32(config["CachingDays"]);
            return demo1;
        }

        public SiteLocaleModel GetSiteLocal()
        {
            var siteLocal = new SiteLocaleModel(config["SiteLocale:ENG"], config["SiteLocale:TH"]);
            return siteLocal;
        }
    }
}
