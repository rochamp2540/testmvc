﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMVC.Models;

namespace TestMVC.Configurations
{
    public interface IConfiguratonContext
    {
        SiteLocaleModel GetSiteLocal();

        int GetCachingDays();
    }
}
