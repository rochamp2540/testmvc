﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestMVC.Configurations;
using TestMVC.Models;
using TestMVC.ViewModels;

namespace TestMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguratonContext configurationContext;
        //private readonly IConfiguration config;
        //public readonly List<A> RowDataList = new List<A>
        //{
        //     new A
        //        {
        //            Number = 1,
        //            Name = "Name1",
        //            Price = 20,
        //            Date = new DateTime(2000,1,10)
        //        },
        //        new A
        //        {
        //            Number = 2,
        //            Name = "Name2",
        //            Price = 20,
        //            Date = new DateTime(2000,1,10)
        //        },
        //        new A
        //        {
        //            Number = 2,
        //            Name = "Name2",
        //            Price = 20,
        //            Date = new DateTime(2000,1,10)
        //        },
        //};

        public HomeController(ILogger<HomeController> logger, 
            IConfiguration config, IConfiguratonContext configurationContext)
        {
            _logger = logger;
            this.configurationContext = configurationContext;
            //this.config = config;
        }

        public IActionResult Index()
        {
            //var model = new UserViewModel();
            //model.Name = "Thanapath";
            var model = new UserViewModel
            {
                Name = "Thanapath",
                Options = new List<SelectListItem>
                    {
                        new SelectListItem("Option 1","1"),
                        new SelectListItem("Option 2","2"),
                        new SelectListItem("Option 3","3")
                    }
            };
            return View(model);
        }

        public IActionResult Link(int id)
        {
            return View("Index");
        }

        [HttpPost]
        public IActionResult Index(UserViewModel model)
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult WorkTable(WorkShopTableModel serch)
        {

            var model = new List<A>
            {
                new A
                {
                    Number = 1,
                    Name = "Name1",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },
                new A
                {
                    Number = 2,
                    Name = "Name2",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },
                new A
                {
                    Number = 2,
                    Name = "Name2",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },

            };

            WorkShopTableModel workShopTableModel = new WorkShopTableModel()
            {
                RowList = model,
            };

            return View("WorkShopTable", workShopTableModel);
        }


        public IActionResult WorkTable()
        {
            //var siteLocal = new SiteLocaleModel(config["SiteLocale:ENG"], config["SiteLocale:TH"]);
            //var demo = config["Demo:Cat"].ToString();
            //var demo1 = Convert.ToInt32(config["CachingDays"]);
            var siteLocal = configurationContext.GetSiteLocal();

            //WorkShopTableModel workShopTableModel = new WorkShopTableModel();
            //workShopTableModel.RowList = RowDataList;

            var model = new List<A>
            {
                new A
                {
                    Number = 1,
                    Name = "Name1",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },
                new A
                {
                    Number = 2,
                    Name = "Name2",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },
                new A
                {
                    Number = 2,
                    Name = "Name2",
                    Price = 20,
                    Date = new DateTime(2000,1,10)
                },

            };

            WorkShopTableModel workShopTableModel = new WorkShopTableModel()
            {
                RowList = model,
            };

            return View("WorkShopTable", workShopTableModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
