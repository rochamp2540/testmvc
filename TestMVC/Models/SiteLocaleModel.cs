﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestMVC.Models
{
    public class SiteLocaleModel
    {
        public SiteLocaleModel(String Eng, String TH)
        {
            this.Engg = Eng;
            this.THH = TH;
        }
        public String Engg { get; set; }
        public String THH { get; set; }
    }
}
