﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMVC.ViewModels;

namespace TestMVC.Validations
{
    public class WorkShopTableModelValidator : AbstractValidator<WorkShopTableModel>
    {
        public WorkShopTableModelValidator()
        {
            RuleFor(x => x.SearchName).NotEmpty();
        }
    }
}
