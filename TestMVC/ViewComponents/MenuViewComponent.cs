﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestMVC.ViewModels;

namespace TestMVC.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var model = new MenuViewModel
            {
                Username = "Thanapath Yam"
            };
            return View(model);
        }
    }
}
