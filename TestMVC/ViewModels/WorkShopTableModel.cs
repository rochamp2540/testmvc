﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestMVC.ViewModels
{
    public class WorkShopTableModel
    {
        //[Required(ErrorMessage = "Input is null")]
        public string SearchName { get; set; }
        public List<A> RowList { get; set; }
    }
    public class A
    {
        public int Number { get; set; }
        public String Name { get; set; }
        public int Price { get; set; }
        public DateTime Date { get; set; }
    }
}
